#ifndef RESPONSE_H
#define RESPONSE_H

#include <string>
#include <map>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

class Response {
public:
    Response();
    std::string* getResponseStr();

    void setCodeAndMsg(int code, const char* msg);
    void setType(const char* type);
    void setContent(const char* content);
    void setContent(std::string* content);
    void setJsonContent(std::map<std::string, std::string>* valuesMap);
    void setJsonContent(boost::property_tree::ptree valuesTree);
    void loadTemplate(const char* tpl_path, std::map<std::string, std::string>* vars);

    void setRedirect(const char* redirect_link, int code);
    void setError500(const char* err_text);
    void setCache15Days();
    void setCookie(const char* cookie_name, const char* cookie_content, int seconds_to_live);

    void setCompressionOn();
    void setCacheOn();
    void handleCompression();

    int getCode();
private:
    std::string resp_data;
    std::string resp_content;

    int resp_code;
    char resp_code_msg[64];
    char resp_type[20];

    bool gzip_compress;
    bool do_cache;

    std::map<std::string, std::string> headerList;
    std::map<std::string, std::pair<std::string, int>> cookieList;

    void setAutoHeaders();
};

#endif
