#include <string>
#include <map>
#include <boost/regex.hpp>

#define TPL_MAX_INCLUDES 50
#define TPL_MAX_VARS 5000

typedef std::map<std::string, std::string> AssocArray;

class TemplateGen {
public:
    TemplateGen(const char* tpl_name, AssocArray* tpl_params_);
    std::string* getCompiled();

private:
    char main_tpl_name[50];
    std::string template_content;
    AssocArray* tpl_params;

    char* getTemplateFile(const char* tpl_path);
    void resolveIncludes();
    void resolveVars();
};

TemplateGen::TemplateGen(const char* tpl_name, AssocArray* tpl_params_ = NULL){
    strcpy(main_tpl_name, tpl_name);
    const char* read_data = getTemplateFile(tpl_name);
    template_content = std::string(read_data);
    delete read_data;

    tpl_params = tpl_params_;
}

char* TemplateGen::getTemplateFile(const char* tpl_name){
    char tpl_full_path[50];
    sprintf(tpl_full_path, "./public/templates/%s", tpl_name);

    char* tpl_content = get_file_content(tpl_full_path);
    if(!tpl_content) return NULL;
    return tpl_content;
}

std::string* TemplateGen::getCompiled(){
    this->resolveIncludes();
    this->resolveVars();

    return &template_content;
}

void TemplateGen::resolveIncludes(){
    boost::regex expr{"\\{\\% *include '([0-9a-zA-Z_\\-\\.\\/]+)' *\\%\\}"};
    boost::smatch what;

    int repl_limit = TPL_MAX_INCLUDES;
    while(boost::regex_search(template_content, what, expr) && repl_limit--){
        // what[0] - grupul total
        // what[1] - grupul cu numele

        char* sub_template_content = getTemplateFile(what.str(1).c_str());
        if(sub_template_content){
            template_content.replace(template_content.find(what[0]), what[0].length(), sub_template_content);
        }
        else{
            template_content.replace(template_content.find(what[0]), what[0].length(), "");
        }
    }
}
void TemplateGen::resolveVars(){
    boost::regex expr{"\\{{ ?([0-9a-zA-Z_\\.]+) ?}}"};
    boost::smatch what;

    int repl_limit = TPL_MAX_VARS;
    char* var_content;
    while(boost::regex_search(template_content, what, expr) && repl_limit--){
        // what[0] - grupul total
        // what[1] - grupul cu numele

        if(tpl_params != NULL && tpl_params->count(what.str(1))){
            std::string val_to_repl = tpl_params->at(what.str(1));
            template_content.replace(template_content.find(what[0]), what[0].length(), escape_html(val_to_repl));
        }
        else{
            printf("Template error: cannot find var %s.\n", what.str(1).c_str());
            template_content.replace(template_content.find(what[0]), what[0].length(), "");
        }
    }
}
