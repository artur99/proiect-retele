#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>

#include "MainHandler.cpp"
#include "Model.h"

#define MAX_REQ_LEN 10*1024*1024 // 10 MB

void init_db(){
    Model* model = new Model();

    if(model->doInitialJobs()){
        printf("Tables created successfuly...\n");
    }
    else{
        printf("DB building error...\n");
    }
    delete model;
}

int main(){
    struct sockaddr_in server, client;
    int sd;

    if((sd = socket(AF_INET, SOCK_STREAM, 0)) == -1){
        perror("Error running socket().\n");
        return 1;
    }

    bzero(&server, sizeof(server));
    bzero(&client, sizeof(client));


    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port = htons(80);

    if(bind(sd, (struct sockaddr*)&server, sizeof(struct sockaddr)) == -1){
        perror("Error running bind().\n");
       return 2;
    }

    if(listen(sd, 1) == -1){
        perror("Error running listen().\n");
        return 3;
    }

    init_db();

    printf("Port 80 is up. Waiting for clients...\n");


    int client_h;
    socklen_t length = sizeof(client);

    char* req_msg = new char[MAX_REQ_LEN + 1];
    // char* res_msg = new char[MAX_RES_LEN + 1];
    unsigned int req_len = 0;
    unsigned int res_len = 0;

    MainHandler* request_handler = new MainHandler();


    while(1){
       client_h = accept(sd, (struct sockaddr *) &client, &length);

       if(client_h < 0){
           perror("Error running accept().\n");
           continue;
       }

       int pid;
       if((pid = fork()) == -1) {
           close(client_h);
           continue;
       }
       else if(pid > 0) {
           // Server, forked, continuing work
           close(client_h);
           continue;
       }
       else if(pid == 0) {
           // Client-serving child process
           close(sd);

           req_len = read(client_h, req_msg, MAX_REQ_LEN);

           if(req_len < 0){
               perror("Reading data from client failed.\n");
               close(client_h);
               return 4;
           }

           request_handler->handleRequest(req_msg);


           if(write(client_h, request_handler->getResponseStr(), request_handler->getResponseLen()) < 0) {
               perror("Error sending response to client.\n");
               return 5;
           }
           else {
               // printf("Client serving succeed.\n");
           }

           close(client_h);
           exit(0);
       }

    }
}
