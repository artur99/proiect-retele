#ifndef UTILS_H
#define UTILS_H

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <ctype.h>
#include <string>
#include <utility>


void get_trimmed_until_space(char* &source, char* dest, int max_len = 100){
    while(*source == ' ') source++;
    while(*source && *source != ' ' && *source != '\n' && *source != '\r' && max_len > 0){
        *dest = *source;
        dest++;
        max_len--;
        source++;
    }
    *dest = 0;
}

void get_trimmed_until_comma(const char* &source, char* dest, int max_len = 100){
    while(*source == ' ') source++;
    while(*source && *source != ',' && *source != '\n' && *source != '\r' && max_len > 0){
        *dest = *source;
        dest++;
        max_len--;
        source++;
    }
    while(*source == ' ') source++;

    *dest = 0;
}

char* get_file_content(const char* file_path){
    FILE* fh;
    char* buffer;
    struct stat st;

    if(access(file_path, R_OK) == -1){
        return NULL;
    }

    fh = fopen(file_path, "rb");
    if(!fh){
        return NULL;
    }

    fstat(fileno(fh), &st);
    buffer = new char[st.st_size + 1];

    fread(buffer, st.st_size, 1, fh);
    fclose(fh);

    buffer[st.st_size] = '\0';

    return buffer;
}

std::pair<char*, int> get_bfile_content(const char* file_path){
    FILE* fh;
    char* buffer;
    struct stat st;

    if(access(file_path, R_OK) == -1){
        return std::pair<char*, int>(NULL, 0);
    }

    fh = fopen(file_path, "rb");
    if(!fh){
        return std::pair<char*, int>(NULL, 0);
    }

    fstat(fileno(fh), &st);
    buffer = new char[st.st_size];

    fread(buffer, st.st_size, 1, fh);
    fclose(fh);

    return std::pair<char*, int>(buffer, st.st_size);
}

int hex_to_int(char x){
    x = tolower(x);
    if(x >= '0' && x <= '9'){
        return x - '0';
    }
    if(x >= 'a' && x <= 'f'){
        return x - 'a' + 10;
    }
    return -1;
}

char int_to_hex(int x){
    if(x < 10)
        return '0' + x;
    return 'a' + x - 10;
}

bool unescape_from_url(char* str, int limit = 99999){
    int c1, c2;
    char* x = str;
    char* i;

    while(*str && limit--){
        if(*str == '%' && str[1] && str[2]){
            c1 = hex_to_int(str[1]);
            c2 = hex_to_int(str[2]);

            if(c1 < 0 || c2 < 0){
                str++;
                continue;
            }

            *str = ((char)(c1))*16 + c2;

            // NEVER trust strcpy :|
            for(i = str + 3; *i; i++){
                *(i-2) = *i;
            }
            *(i-2) = *i; // Copy null


        }
        else if(*str == '+'){
            *str = ' ';
        }
        str++;
    }
}

char getRandAlphaNum(){
    unsigned int x = rand() % (('Z' - 'A') * 2 + 10);
    if(x < 10) return '0' + x;
    x -= 10;
    if(x < ('z' - 'a'))
        return 'a' + x;
    return 'A' + (x - ('z' - 'a'));
}

std::string escape_char(std::string html, std::string chr, std::string esc){
    int loc = 0;
    while ((loc = html.find(chr, loc)) != std::string::npos) {
        html.replace(loc, chr.size(), esc);
        loc += esc.size();
    }
    return html;
}

std::string escape_html(std::string html){
    html = escape_char(html, "&", "&amp;");
    html = escape_char(html, "<", "&lt;");
    html = escape_char(html, ">", "&gt;");
    return html;
}

#endif
