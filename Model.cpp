#include <string>
#include <map>
#include <cstdlib>
#include <time.h>

#include <sqlite3.h>
#include <boost/regex.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/uuid/sha1.hpp>

#include "Model.h"

#define TMP_DIR "./tmp"
#define SESSION_DIR "./tmp/session"
#define DB_NAME "./tmp/main.db"
#define TOKEN_SIZE 32


Model::Model(){
    user_loaded = false;

    createTmpDirs();

    int rc = sqlite3_open(DB_NAME, &database);
    if(rc != 0){
        db_connected = false;
        printf("Cannot connect to db.\n");
    }
    else{
        db_connected = true;
    }

}

Model::~Model(){
    if(db_connected){
        sqlite3_close(database);
    }
}

bool Model::createTmpDirs(){
    mkdir(TMP_DIR, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(SESSION_DIR, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

    return true;
}

bool Model::doInitialJobs(){
    // CREATE TABLE IF NOT EXISTS users (
    //     id INTEGER PRIMARY KEY AUTOINCREMENT,
    //     email TEXT NOT NULL,
    //     password TEXT NOT NULL,
    //     name TEXT,
    //     signup_time INTEGER,
    //     login_time INTEGER
    // )
    // CREATE TABLE IF NOT EXISTS posts (
    //     id INTEGER PRIMARY KEY AUTOINCREMENT,
    //     user_id INTEGER NOT NULL,
    //     post TEXT,
    //     post_time INTEGER NOT NULL,
    //     post_visibility INTEGER DEFAULT 1 //(1=public, 2=friends, 3=me)
    // );
    // CREATE TABLE IF NOT EXISTS friendships (
    //     id INTEGER PRIMARY KEY AUTOINCREMENT,
    //     user1_id INTEGER NOT NULL,
    //     user2_id INTEGER NOT NULL,
    //     add_time INTEGER NOT NULL
    // );
    // CREATE TABLE IF NOT EXISTS friend_requests (
    //     id INTEGER PRIMARY KEY AUTOINCREMENT,
    //     user1_id INTEGER NOT NULL,
    //     user2_id INTEGER NOT NULL,
    //     req_time INTEGER NOT NULL
    // );

    const char* sql_1 = "CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT, email TEXT NOT NULL, password TEXT NOT NULL, name TEXT, signup_time INTEGER, login_time INTEGER);";
    const char* sql_2 = "CREATE TABLE IF NOT EXISTS posts (id INTEGER PRIMARY KEY AUTOINCREMENT, user_id INTEGER NOT NULL, post TEXT, post_time INTEGER NOT NULL, post_visibility INTEGER DEFAULT 0);";
    const char* sql_3 = "CREATE TABLE IF NOT EXISTS friendships (id INTEGER PRIMARY KEY AUTOINCREMENT, user1_id INTEGER NOT NULL, user2_id INTEGER NOT NULL, add_time INTEGER NOT NULL);";
    const char* sql_4 = "CREATE TABLE IF NOT EXISTS friend_requests (id INTEGER PRIMARY KEY AUTOINCREMENT, user1_id INTEGER NOT NULL, user2_id INTEGER NOT NULL, req_time INTEGER NOT NULL);";

    bool all_ok = true;
    if(sqlite3_exec(database, sql_1, NULL, NULL, NULL) != SQLITE_OK){
        all_ok = false;
    }
    if(sqlite3_exec(database, sql_2, NULL, NULL, NULL) != SQLITE_OK){
        all_ok = false;
    }
    if(sqlite3_exec(database, sql_3, NULL, NULL, NULL) != SQLITE_OK){
        all_ok = false;
    }
    if(sqlite3_exec(database, sql_4, NULL, NULL, NULL) != SQLITE_OK){
        all_ok = false;
    }

    return all_ok;
}

bool Model::isLoggedIn(){
    if(!cookieList->count("token"))
        return false;

    return getSessionForToken(cookieList->at("token"));
}
bool Model::feedCookies(std::map<std::string, std::string>* cookieList_){
    cookieList = cookieList_;
    return true;
}

bool Model::getSessionForToken(std::string token){
    char buffer[501];

    if(user_loaded)
        return true;

    // Session File Structure
    // 1. Unix_expire (int)
    // 2. User ID
    // 3. Email
    // 4. Name

    if(!boost::regex_match(token, boost::regex("[0-9a-zA-Z]+")))
        return false;

    snprintf(buffer, 500, "%s/%s.json", SESSION_DIR, token.c_str());

    try {
        boost::property_tree::ptree pt;
        boost::property_tree::read_json(buffer, pt);

        auto unix_expire = pt.get<unsigned long long>("expire");

        if(unix_expire < time(NULL)){
            return false;
        }

        user_id = pt.get<int>("id");
        user_email = pt.get<std::string>("email");
        user_name = pt.get<std::string>("name");

        user_loaded = true;
        current_token = token;
    } catch(std::exception const &e){
        return false;
    }

    return true;
}

bool Model::loginUser(std::string email, std::string pass){
    if(!db_connected) return false;

    std::string pass_h = sha1(pass);

    sqlite3_stmt *stmt;

    sqlite3_prepare_v2(database, "SELECT id, name, email FROM users WHERE email = ? AND password = ? LIMIT 1", -1, &stmt, NULL);
    sqlite3_bind_text(stmt, 1, email.c_str(), -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 2, pass_h.c_str(), -1, SQLITE_STATIC);

    bool found_user = false;

    while (sqlite3_step(stmt) == SQLITE_ROW){
		int i;
		int num_cols = sqlite3_column_count(stmt);
        if(num_cols != 3){
            found_user = false;
            break;
        }
        user_id = sqlite3_column_int(stmt, 0);
        user_name = std::string((const char *) sqlite3_column_text(stmt, 1));
        user_email = std::string((const char *) sqlite3_column_text(stmt, 2));
        found_user = true;
	}

    sqlite3_finalize(stmt);

    if(found_user){
        boost::property_tree::ptree pt;

        pt.put("email", user_email);
        pt.put("name", user_name);
        pt.put("id", user_id);
        pt.put("expire", (unsigned long long) time(NULL) + 60 * 60 * 24);

        char buffer[501];
        char token[TOKEN_SIZE + 1];

        srand(time(NULL) * (unsigned long long)(&email));

        for(int i = 0; i <= TOKEN_SIZE; i++){
            token[i] = getRandAlphaNum();
        }
        token[TOKEN_SIZE + 1] = '\0';

        snprintf(buffer, 500, "%s/%s.json", SESSION_DIR, token);

        boost::property_tree::write_json(buffer, pt);
        current_token = token;

        return true;
    }

    return false;
}

std::string* Model::getToken(){
    if(current_token.size()){
        return &current_token;
    }
    return NULL;
}

bool Model::logoutUser(){
    if(!cookieList->count("token"))
        return false;


    char session_fname[501];
    snprintf(session_fname, 500, "%s/%s.json", SESSION_DIR, cookieList->at("token").c_str());

    remove(session_fname);
}

std::string Model::sha1(std::string str){
    boost::uuids::detail::sha1 sha_hasher;

    sha_hasher.process_bytes(str.data(), str.size());
    unsigned digest[5];

    sha_hasher.get_digest(digest);

    char buffer[41];
    buffer[40] = '\0';
    for(int i = 0; i < 5; i++){

        for(int j = 7; j >= 0; j --){
            buffer[i*8 + (7-j)] = int_to_hex((digest[i] >> (j * 4)) & 0xF);
        }
    }

    return std::string(buffer);
}

bool Model::emailExists(std::string email){
    if(!db_connected) return true;

    sqlite3_stmt *stmt;

    sqlite3_prepare_v2(database, "SELECT id FROM users WHERE email = ? LIMIT 1", -1, &stmt, NULL);
    sqlite3_bind_text(stmt, 1, email.c_str(), -1, SQLITE_STATIC);
    bool found_email = false;

    while (sqlite3_step(stmt) == SQLITE_ROW){
        found_email = true;
    }

    sqlite3_finalize(stmt);

    return found_email;
}

bool Model::signupUser(std::string email, std::string name, std::string pass){
    if(!db_connected) return false;

    std::string pass_h = sha1(pass);

    sqlite3_stmt *stmt;

    sqlite3_prepare_v2(database, "INSERT INTO users(email, name, password) VALUES(?, ?, ?)", -1, &stmt, NULL);
    sqlite3_bind_text(stmt, 1, email.c_str(), -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 2, name.c_str(), -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 3, pass_h.c_str(), -1, SQLITE_STATIC);

    if(sqlite3_step(stmt) != SQLITE_DONE){
        printf("Query error: %s\n", sqlite3_errmsg(database));
        sqlite3_finalize(stmt);
        return false;
    }
    sqlite3_finalize(stmt);
    return true;
}

std::string Model::getUserId(){
    return std::to_string(user_id);
}
std::string Model::getUserEmail(){
    return user_email;
}
std::string Model::getUserName(){
    return user_name;
}
bool Model::addPost(std::string content, int visibility){
    if(!db_connected) return false;

    sqlite3_stmt *stmt;

    sqlite3_prepare_v2(database, "INSERT INTO posts(user_id, post, post_time, post_visibility) VALUES(?, ?, strftime('%s', 'now'), ?)", -1, &stmt, NULL);
    sqlite3_bind_int(stmt, 1, user_id);
    sqlite3_bind_text(stmt, 2, content.c_str(), -1, SQLITE_STATIC);
    sqlite3_bind_int(stmt, 3, visibility);

    if(sqlite3_step(stmt) != SQLITE_DONE){
        printf("Query error: %s\n", sqlite3_errmsg(database));
        sqlite3_finalize(stmt);
        return false;
    }
    sqlite3_finalize(stmt);
    return true;
}

boost::property_tree::ptree Model::getPosts(int get_user_id = 0){
    boost::property_tree::ptree pt;
    boost::property_tree::ptree post_list;

    if(!db_connected) return pt;

    sqlite3_stmt *stmt;

    if(get_user_id == 0){
        sqlite3_prepare_v2(
            database,
            "SELECT post, post_visibility, post_time, users.id, users.name "
            "FROM posts "
            "INNER JOIN users ON users.id = posts.user_id "
            "WHERE user_id = ? OR "
            "("
                "(post_visibility = 1 OR post_visibility = 2) AND "
                "EXISTS (SELECT id FROM friendships WHERE user1_id = ? AND user2_id = user_id) "
            ") "
            "ORDER BY post_time DESC LIMIT 10",
            -1, &stmt, NULL
        );
        sqlite3_bind_int(stmt, 1, user_id);
        sqlite3_bind_int(stmt, 2, user_id);
    }
    else {
        sqlite3_prepare_v2(
            database,
            "SELECT post, post_visibility, post_time, users.id, users.name "
            "FROM posts "
            "INNER JOIN users ON users.id = posts.user_id "
            "WHERE user_id = ? AND ( "
                "post_visibility = 1 OR "
                "(post_visibility = 2 AND (user_id = ? OR EXISTS (SELECT id FROM friendships WHERE user1_id = ? AND user2_id = user_id))) OR " // add there an OR is friends with
                "(post_visibility = 3 AND user_id = ?) " // only me posts
            ") "
            "ORDER BY post_time DESC LIMIT 10",
            -1, &stmt, NULL
        );
        sqlite3_bind_int(stmt, 1, get_user_id);
        sqlite3_bind_int(stmt, 2, user_id);
        sqlite3_bind_int(stmt, 3, user_id);
        sqlite3_bind_int(stmt, 4, user_id);

    }

    int num_cols;

    while (sqlite3_step(stmt) == SQLITE_ROW){
        boost::property_tree::ptree post;
        num_cols = sqlite3_column_count(stmt);
        if(num_cols != 5){
            continue;
        }

        post.put("post", std::string((const char *) sqlite3_column_text(stmt, 0)));
        post.put("visibility", std::string((const char *) sqlite3_column_text(stmt, 1)));
        post.put("post_time", std::to_string(sqlite3_column_int(stmt, 2)));
        post.put("user_id", std::to_string(sqlite3_column_int(stmt, 3)));
        post.put("user_name", std::string((const char *) sqlite3_column_text(stmt, 4)));

        post_list.push_back(std::make_pair("", post));
    }

    sqlite3_finalize(stmt);

    pt.add_child("posts", post_list);

    return pt;
}

boost::property_tree::ptree Model::searchUser(std::string text){
    boost::property_tree::ptree pt;
    boost::property_tree::ptree user_results;

    if(!db_connected) return pt;

    std::string content = std::string("%") + text + std::string("%");
    std::replace(content.begin(), content.end(), ' ', '%');

    sqlite3_stmt *stmt;

    sqlite3_prepare_v2(database, "SELECT u.id, u.name, EXISTS (SELECT id FROM friendships WHERE user1_id = ? AND user2_id = u.id) FROM users u WHERE lower(u.name) LIKE lower(?) LIMIT 10", -1, &stmt, NULL);
    sqlite3_bind_int(stmt, 1, user_id);
    sqlite3_bind_text(stmt, 2, content.c_str(), -1, SQLITE_STATIC);

    int num_cols;

    while (sqlite3_step(stmt) == SQLITE_ROW){
        boost::property_tree::ptree user;
        num_cols = sqlite3_column_count(stmt);
        if(num_cols != 3){
            continue;
        }

        user.put("user_id", std::to_string(sqlite3_column_int(stmt, 0)));
        user.put("user_name", std::string((const char *) sqlite3_column_text(stmt, 1)));
        user.put("friends", std::string((const char *) sqlite3_column_text(stmt, 2)));

        user_results.push_back(std::make_pair("", user));
    }

    sqlite3_finalize(stmt);

    pt.add_child("results", user_results);

    return pt;
}

std::map<std::string, std::string> Model::getUserInfoById(int get_user_id){
    std::map<std::string, std::string> data;

    sqlite3_stmt *stmt;

    sqlite3_prepare_v2(database, "SELECT u.id, u.name, EXISTS (SELECT id FROM friendships WHERE user1_id = ? AND user2_id = u.id) FROM users u WHERE u.id = ? LIMIT 1", -1, &stmt, NULL);
    sqlite3_bind_int(stmt, 1, user_id);
    sqlite3_bind_int(stmt, 2, get_user_id);

    while(sqlite3_step(stmt) == SQLITE_ROW){
        int num_cols = sqlite3_column_count(stmt);
        if(num_cols != 3){
            return data;
        }

        data["id"] = std::to_string(sqlite3_column_int(stmt, 0));
        data["name"] = std::string((const char *) sqlite3_column_text(stmt, 1));
        data["friend"] = std::string((const char *) sqlite3_column_text(stmt, 2));
    }

    sqlite3_finalize(stmt);

    sqlite3_prepare_v2(database, "SELECT count(*) FROM posts WHERE user_id = ?", -1, &stmt, NULL);
    sqlite3_bind_int(stmt, 1, get_user_id);

    while(sqlite3_step(stmt) == SQLITE_ROW){
        int num_cols = sqlite3_column_count(stmt);
        if(num_cols != 1){
            data["post_count"] = std::to_string(0);
            return data;
        }

        data["post_count"] = std::to_string(sqlite3_column_int(stmt, 0));
    }


    return data;
}

bool Model::deleteFriend(int friend_id){
    if(!db_connected) return false;

    sqlite3_stmt *stmt;

    sqlite3_prepare_v2(database, "DELETE FROM friendships WHERE (user1_id = ? AND user2_id = ?) OR (user1_id = ? AND user2_id = ?)", -1, &stmt, NULL);
    sqlite3_bind_int(stmt, 1, friend_id);
    sqlite3_bind_int(stmt, 2, user_id);
    sqlite3_bind_int(stmt, 3, user_id);
    sqlite3_bind_int(stmt, 4, friend_id);

    if(sqlite3_step(stmt) != SQLITE_DONE){
        printf("Query error: %s\n", sqlite3_errmsg(database));
        sqlite3_finalize(stmt);
        return false;
    }
    sqlite3_finalize(stmt);
    return true;
}
bool Model::sendFriendRequest(int person_id){
    if(!db_connected) return false;

    sqlite3_stmt *stmt;

    sqlite3_prepare_v2(database, "INSERT INTO friend_requests(user1_id, user2_id, req_time) VALUES(?, ?, strftime('%s', 'now'))", -1, &stmt, NULL);
    sqlite3_bind_int(stmt, 1, user_id);
    sqlite3_bind_int(stmt, 2, person_id);

    if(sqlite3_step(stmt) != SQLITE_DONE){
        printf("Query error: %s\n", sqlite3_errmsg(database));
        sqlite3_finalize(stmt);
        return false;
    }
    sqlite3_finalize(stmt);
    return true;
}

boost::property_tree::ptree Model::getFriendRequests(){
    boost::property_tree::ptree pt;
    boost::property_tree::ptree user_results;

    if(!db_connected) return pt;


    sqlite3_stmt *stmt;

    sqlite3_prepare_v2(database, "SELECT DISTINCT user1_id, req_time, name from friend_requests INNER JOIN users ON friend_requests.user1_id = users.id WHERE user2_id = ? GROUP BY user1_id", -1, &stmt, NULL);
    sqlite3_bind_int(stmt, 1, user_id);

    int num_cols;

    while (sqlite3_step(stmt) == SQLITE_ROW){
        boost::property_tree::ptree user;
        num_cols = sqlite3_column_count(stmt);
        if(num_cols != 3){
            continue;
        }

        user.put("user_id", std::to_string(sqlite3_column_int(stmt, 0)));
        user.put("req_time", std::to_string(sqlite3_column_int(stmt, 1)));
        user.put("name", std::string((const char*) sqlite3_column_text(stmt, 2)));

        user_results.push_back(std::make_pair("", user));
    }

    sqlite3_finalize(stmt);

    pt.add_child("requests", user_results);

    return pt;
}

bool Model::acceptFriendRequest(int person_id){
    if(!db_connected) return false;

    sqlite3_stmt *stmt;

    sqlite3_prepare_v2(database, "SELECT id FROM friend_requests WHERE user1_id = ?", -1, &stmt, NULL);
    sqlite3_bind_int(stmt, 1, person_id);

    bool fq_exists = false;

    while(sqlite3_step(stmt) == SQLITE_ROW){
        fq_exists = true;
    }

    sqlite3_finalize(stmt);

    if(!fq_exists) return false;

    sqlite3_prepare_v2(database, "INSERT INTO friendships(user1_id, user2_id, add_time) VALUES(?, ?, strftime('%s', 'now'))", -1, &stmt, NULL);
    sqlite3_bind_int(stmt, 1, user_id);
    sqlite3_bind_int(stmt, 2, person_id);

    if(sqlite3_step(stmt) != SQLITE_DONE){
        printf("Query error: %s\n", sqlite3_errmsg(database));
        sqlite3_finalize(stmt);
        return false;
    }
    sqlite3_finalize(stmt);

    sqlite3_prepare_v2(database, "INSERT INTO friendships(user1_id, user2_id, add_time) VALUES(?, ?, strftime('%s', 'now'))", -1, &stmt, NULL);
    sqlite3_bind_int(stmt, 1, person_id);
    sqlite3_bind_int(stmt, 2, user_id);

    if(sqlite3_step(stmt) != SQLITE_DONE){
        printf("Query error: %s\n", sqlite3_errmsg(database));
        sqlite3_finalize(stmt);
        return false;
    }
    sqlite3_finalize(stmt);


    sqlite3_prepare_v2(database, "DELETE FROM friend_requests WHERE user1_id = ? AND user2_id = ?", -1, &stmt, NULL);
    sqlite3_bind_int(stmt, 1, person_id);
    sqlite3_bind_int(stmt, 2, user_id);

    if(sqlite3_step(stmt) != SQLITE_DONE){
        printf("Query error: %s\n", sqlite3_errmsg(database));
        sqlite3_finalize(stmt);
        return false;
    }
    sqlite3_finalize(stmt);

    return true;
}

boost::property_tree::ptree Model::getFriendList(){
    boost::property_tree::ptree pt;
    boost::property_tree::ptree user_results;

    if(!db_connected) return pt;


    sqlite3_stmt *stmt;

    sqlite3_prepare_v2(database, "SELECT DISTINCT users.id, users.name FROM users INNER JOIN friendships ON (friendships.user2_id = users.id) WHERE friendships.user1_id = ?", -1, &stmt, NULL);
    sqlite3_bind_int(stmt, 1, user_id);

    int num_cols;

    while (sqlite3_step(stmt) == SQLITE_ROW){
        boost::property_tree::ptree user;
        num_cols = sqlite3_column_count(stmt);
        if(num_cols != 2){
            continue;
        }

        user.put("user_id", std::to_string(sqlite3_column_int(stmt, 0)));
        user.put("name", std::string((const char*) sqlite3_column_text(stmt, 1)));

        user_results.push_back(std::make_pair("", user));
    }

    sqlite3_finalize(stmt);

    pt.add_child("friend_list", user_results);

    return pt;
}

std::string Model::getFriendRequestsCount(){
    if(!db_connected) return std::to_string(0);
    sqlite3_stmt *stmt;

    sqlite3_prepare_v2(database, "SELECT COUNT(DISTINCT user1_id) FROM friend_requests WHERE user2_id = ?", -1, &stmt, NULL);
    sqlite3_bind_int(stmt, 1, user_id);

    int num_cols;
    int result_count = 0;

    while (sqlite3_step(stmt) == SQLITE_ROW){
        boost::property_tree::ptree user;
        num_cols = sqlite3_column_count(stmt);
        if(num_cols != 1){
            continue;
        }

        result_count = sqlite3_column_int(stmt, 0);
    }

    sqlite3_finalize(stmt);

    return std::to_string(result_count);
}

void Model::setGuest(){
    user_id = 0;
    user_name = "";
}
