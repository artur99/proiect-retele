#include <string>
#include <sstream>
#include <map>
#include <iostream>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "utils.h"

#include "Response.h"
#include "TemplateGen.cpp"

#define HTTP_VERSION "HTTP/1.1"
#define SERVER_NAME "ServerRetele_2019/1.0"

typedef std::map<std::string, std::string> AssocArray;

Response::Response(){
    resp_code = 200;
    gzip_compress = false;
    do_cache = false;
    strcpy(resp_code_msg, "OK");
    strcpy(resp_type, "html");
}

void Response::setRedirect(const char* redirect_link, int code = 302){
    resp_code = code;
    strcpy(resp_code_msg, "Moved Temporary");
    headerList[std::string("Location")] = std::string(redirect_link);
}

void Response::setError500(const char* err_text = "?"){
    resp_code = 500;
    setType("html");
    char error[200];
    sprintf(error, "Server error (%s). Please check the logs.", err_text);
    setContent(error);
}

void Response::setCodeAndMsg(int code, const char* msg){
    resp_code = code;
    if(msg){
        strcpy(resp_code_msg, msg);
    }
}

void Response::setType(const char* type){
    strcpy(resp_type, type);

    if(strcmp(type, "html") == 0){
        headerList[std::string("Content-Type")] = std::string("text/html; charset=UTF-8");
    }
    else if(strcmp(type, "json") == 0){
        headerList[std::string("Content-Type")] = std::string("application/json; charset=UTF-8");
    }
    else if(strcmp(type, "woff") == 0){
        setCache15Days();
        headerList[std::string("Content-Type")] = std::string("font/woff");
    }
    else if(strcmp(type, "woff2") == 0){
        setCache15Days();
        headerList[std::string("Content-Type")] = std::string("font/woff2");
    }
    else if(strcmp(type, "ttf") == 0){
        setCache15Days();
        headerList[std::string("Content-Type")] = std::string("font/ttf");
    }
    else if(strcmp(type, "js") == 0){
        setCache15Days();
        headerList[std::string("Content-Type")] = std::string("application/javascript; charset=UTF-8");
    }
    else if(strcmp(type, "css") == 0){
        setCache15Days();
        headerList[std::string("Content-Type")] = std::string("text/css; charset=UTF-8");
    }
    else if(strcmp(type, "map") == 0){
        setCache15Days();
    }
}

void Response::setCache15Days(){
    if(do_cache)
        headerList[std::string("Cache-Control")] = std::string("public, max-age=21600");
}

void Response::setAutoHeaders(){
    headerList[std::string("Content-Length")] = std::to_string(resp_content.length());
    headerList[std::string("Server")] = std::string(SERVER_NAME);
}

void Response::setContent(const char* content){
    resp_content += content;
}
void Response::setContent(std::string* content){
    resp_content += *content;
}
void Response::setJsonContent(AssocArray* valuesMap){
    boost::property_tree::ptree json_values;
    for(auto el: *valuesMap)
        json_values.put(el.first, el.second);

    this->setJsonContent(json_values);
}

void Response::setJsonContent(boost::property_tree::ptree valuesTree){
    std::ostringstream buffer;
    boost::property_tree::write_json(buffer, valuesTree, false);

    resp_content += buffer.str();

    setType("json");
}

std::string* Response::getResponseStr(){
    handleCompression();
    setAutoHeaders();

    char buffer[200];
    snprintf(buffer, 150, "%s %d %s\n", HTTP_VERSION, resp_code, resp_code_msg);
    resp_data += buffer;

    for(auto const header: headerList) {
        resp_data += header.first;
        resp_data += ": ";
        resp_data += header.second;
        resp_data += "\n";
    }

    if(cookieList.size() > 0){
        for(auto const cookieItem: cookieList){
            snprintf(buffer, 200, "Set-Cookie: %s=%s; Max-Age=%d; Path=/\n", cookieItem.first.c_str(), cookieItem.second.first.c_str(), cookieItem.second.second);
            resp_data += buffer;
        }
    }


    resp_data += "\n";
    resp_data += resp_content;

    return &resp_data;
}

void Response::loadTemplate(const char* tpl_path, AssocArray* vars = NULL){
    TemplateGen* template_gen = new TemplateGen(tpl_path, vars);

    std::string* tpl_content = template_gen->getCompiled();

    if(!tpl_content){
        printf("Error. Cannot read template: %s .\n", tpl_path);
        setError500("Cannot read template");
        return;
    }
    setType("html");
    setContent(tpl_content);
}

void Response::setCookie(const char* cookie_name, const char* cookie_content, int seconds_to_live){
    cookieList[std::string(cookie_name)] = std::pair<std::string, int>(std::string(cookie_content), seconds_to_live);
}

void Response::setCompressionOn(){
    gzip_compress = true;
}

void Response::setCacheOn(){
    do_cache = true;
}

void Response::handleCompression(){
    if(!gzip_compress) return;
    namespace bio = boost::iostreams;

    std::stringstream original_data(resp_content);
    std::stringstream compressed_data;
	bio::filtering_streambuf<bio::input> out;

	out.push(bio::gzip_compressor(bio::gzip_params(bio::gzip::best_compression)));
	out.push(original_data);
	bio::copy(out, compressed_data);

    resp_content = compressed_data.str();

    headerList[std::string("Content-Encoding")] = std::string("gzip");
}


int Response::getCode(){
    return resp_code;
}
