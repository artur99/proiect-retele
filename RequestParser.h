#ifndef REQUEST_PARSER_H
#define REQUEST_PARSER_H

#define MAX_REQUEST_HEADER_LEN 1024

class RequestParser {
public:
    RequestParser();
    void beginParsing(char* &data_cursor);
    void checkAcceptedEncodings();

    char* getReqPath();
    char* getReqFullPath();
    char* getReqType();

    std::map<std::string, std::string>* getBodyParams();
    std::map<std::string, std::string>* getCookies();

    bool acceptsEncoding(const char* encoding);

    std::map<std::string, std::string> headerList;
    std::list<std::string> acceptedEncodingList;
    std::map<std::string, std::string> bodyList;
    std::map<std::string, std::string> cookieList;
private:
    bool parseMainHeader(char* &data_cursor);
    bool parseBody(char* &data_cursor);
    bool parseLine(char* &data_cursor);
    void splitGetParams();
    void parseCookies();

    char request_type[10];
    char request_path[1024];
    char request_full_path[1024];
    char request_query_params[1024];
    char request_protocol[10];

    char* raw_request_pointer;
};

#endif
