#include <string>
#include <map>
#include <list>
#include <algorithm>
#include <iostream>

#include "utils.h"
#include "RequestParser.h"



RequestParser::RequestParser(){
    request_type[0] = '\0';
    request_path[0] = '\0';
    request_full_path[0] = '\0';
    request_query_params[0] = '\0';
    request_protocol[0] = '\0';
}

char* RequestParser::getReqPath(){
    return request_path;
}
char* RequestParser::getReqFullPath(){
    return request_full_path;
}

char* RequestParser::getReqType(){
    return request_type;
}

std::map<std::string, std::string>* RequestParser::getBodyParams(){
    return &bodyList;
}

std::map<std::string, std::string>* RequestParser::getCookies(){
    return &cookieList;
}

void RequestParser::beginParsing(char* &data_cursor){
    raw_request_pointer = data_cursor;
    int i = 0;
    bool new_line = 0;
    int beg_of_line = 0;
    int colon_loc = 0;
    bool found_colon = 0;

    this->parseMainHeader(data_cursor);

    while(*data_cursor){
        if(*data_cursor == '\r'){
            // empty line, end of header
            data_cursor++;
            if(*data_cursor == '\n') data_cursor++;
            break;
        }
        else if(*data_cursor == '\n'){
            data_cursor++;
            break;
        }
        this->parseLine(data_cursor);
    }

    this->parseCookies();

    this->parseBody(data_cursor);

    // for(auto mapel: bodyList){
    //     printf("Map el: %s -> %s\n", mapel.first.c_str(), mapel.second.c_str());
    // }

    checkAcceptedEncodings();
    // return true;
}

bool RequestParser::parseMainHeader(char* &data_cursor){
    short header_step = 0;
    int i = 0;

    while(*data_cursor){
        if(*data_cursor == '\n'){
            data_cursor++;
            break;
        }

        if(header_step == 0){
            get_trimmed_until_space(data_cursor, request_type, sizeof(request_type) - 1);
            header_step = 1;
        }
        else if(header_step == 1){
            get_trimmed_until_space(data_cursor, request_full_path, sizeof(request_path) - 1);
            header_step = 2;
        }
        else if(header_step == 2){
            get_trimmed_until_space(data_cursor, request_protocol, sizeof(request_protocol) - 1);
            header_step = 3;
        }

        data_cursor++;
    }

    splitGetParams();

    for(int i = 0; request_type[i]; i++){
        request_type[i] = toupper(request_type[i]);
    }

    if(strlen(request_type) == 0){
        // printf("ERROOOOOR: (%s)\n", raw_request_pointer);
        printf("Malformed request...\n");
        return false;
    }

    // printf("[OK] %s %s\n", request_type, request_full_path);
    return true;
}

void RequestParser::splitGetParams(){
    char* stepper = request_full_path;
    while(*stepper){
        if(*stepper == '?'){
            strncpy(request_path, request_full_path, (stepper - request_full_path));
            strcpy(request_query_params, stepper + 1);
            return;
        }
        stepper++;
    }
    strcpy(request_path, request_full_path);
}

void RequestParser::checkAcceptedEncodings(){
    if(headerList.count(std::string("accept-encoding"))){
        const char* str_cursor = headerList[std::string("accept-encoding")].c_str();
        char enc_name[21];
        std::string enc_name_str;
        while(*str_cursor){
            get_trimmed_until_comma(str_cursor, enc_name, 20);
            enc_name_str = std::string(enc_name);

            if(!enc_name_str.size()) break;
            acceptedEncodingList.push_back(enc_name_str);

            if(!*str_cursor) break;
            str_cursor++;
        }
    }
}

bool RequestParser::acceptsEncoding(const char* encoding){
    return std::find(acceptedEncodingList.begin(), acceptedEncodingList.end(), encoding) != acceptedEncodingList.end();
}

bool RequestParser::parseLine(char* &data_cursor){
    char* header_key = data_cursor;
    char* header_cont = NULL;

    int header_key_len = 0;
    int header_cont_len = 0;

    short passed_colon = 0;

    while(*data_cursor){
        if(*data_cursor == '\n' || *data_cursor == '\r'){
            if(*data_cursor == '\r') data_cursor++;
            // End of line
            header_cont_len = data_cursor - header_cont - 1;

            data_cursor++;
            break;
        }
        else if(passed_colon == 0 && *data_cursor == ':'){
            passed_colon = 1;
            header_key_len = data_cursor - header_key;
        }
        else if(passed_colon == 1 && *data_cursor != ' '){
            passed_colon = 2;
            header_cont = data_cursor;
        }

        data_cursor++;
    }

    if(
        header_cont && header_key &&
        header_key_len <= 0 || header_cont_len < 0 ||
        header_key_len > 50 || header_cont_len > 255
    ){
        // Bad header line
        // printf("Got bad header (%s) (%s)...\n", header_key, header_cont);
        return false;
    }

    std::string str_header_key = std::string(header_key, header_key_len);
    std::string str_header_cont = std::string(header_cont, header_cont_len);
    std::transform(str_header_key.begin(), str_header_key.end(), str_header_key.begin(), ::tolower);

    headerList[str_header_key] = str_header_cont;

    // printf("Header: (%s)[%s]\n", str_header_key.c_str(), headerList[str_header_key].c_str());
    return true;
}


bool RequestParser::parseBody(char* &data_cursor){
    while(*data_cursor){
        const char* param_begin = data_cursor;
        const char* param_end = NULL;
        const char* content_begin = NULL;
        const char* content_end = NULL;

        while(*data_cursor && *data_cursor != '='){
            data_cursor++;
        }
        if(*data_cursor == '='){
            param_end = data_cursor;
            content_begin = ++data_cursor;
            while(*data_cursor && *data_cursor != '&'){
                data_cursor++;
            }
            content_end = data_cursor;
        }
        if(!content_begin){
            param_end = data_cursor;
            content_begin = "";
            content_end = content_begin;
        }

        char* buffer = new char[content_end - content_begin + 1];
        buffer[content_end - content_begin] = '\0';
        strncpy(buffer, content_begin, content_end - content_begin);
        unescape_from_url(buffer);


        bodyList[std::string(param_begin, param_end - param_begin)] = std::string(buffer);

        delete buffer;

        if(*data_cursor)
            data_cursor++;
        else
            break;
    }
}

void RequestParser::parseCookies(){
    if(!headerList.count("cookie")) return;
    const char* data_cursor = headerList["cookie"].c_str();

    while(*data_cursor){
        const char* param_begin = NULL;
        const char* param_end = NULL;
        const char* content_begin = NULL;
        const char* content_end = NULL;

        // Remove empty space before any string
        while(*data_cursor == ' ' || *data_cursor == '\t')
            data_cursor++;

        param_begin = data_cursor;

        // Go until '='
        while(*data_cursor && *data_cursor != '=')
            data_cursor++;

        // If found an '=', go until ';'
        if(*data_cursor == '='){
            param_end = data_cursor;
            content_begin = ++data_cursor;
            while(*data_cursor && *data_cursor != ';'){
                data_cursor++;
            }
            content_end = data_cursor;
        }
        // If '=' doesn't exist:
        if(!content_begin){
            param_end = data_cursor;
            content_begin = "";
            content_end = content_begin;
        }

        // Decode string after '=' (format: %xx -> char)
        char* buffer = new char[content_end - content_begin + 1];
        buffer[content_end - content_begin] = '\0';
        strncpy(buffer, content_begin, content_end - content_begin);
        unescape_from_url(buffer);


        cookieList[std::string(param_begin, param_end - param_begin)] = std::string(buffer);
        // printf("Cookie: %s=%s\n", std::string(param_begin, param_end - param_begin).c_str(), std::string(buffer).c_str());

        delete buffer;

        if(*data_cursor)
            data_cursor++;
        else
            break;
    }

}
