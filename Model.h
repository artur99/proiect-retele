#ifndef MODEL_H
#define MODEL_H

#include <string>
#include <map>

#include <boost/property_tree/ptree.hpp>

class Model {
public:
    Model();
    ~Model();

    bool doInitialJobs();
    bool createTmpDirs();
    bool feedCookies(std::map<std::string, std::string>* cookieList_);

    bool isLoggedIn();
    bool loginUser(std::string email, std::string password);
    bool signupUser(std::string email, std::string name, std::string pass);
    bool emailExists(std::string email);
    void setGuest();
    bool logoutUser();

    bool addPost(std::string content, int visibility);
    boost::property_tree::ptree getPosts(int get_user_id);

    boost::property_tree::ptree searchUser(std::string text);
    std::map<std::string, std::string> getUserInfoById(int get_user_id);

    bool deleteFriend(int friend_id);
    bool sendFriendRequest(int person_id);
    bool acceptFriendRequest(int person_id);
    boost::property_tree::ptree getFriendRequests();
    boost::property_tree::ptree getFriendList();

    std::string getFriendRequestsCount();


    std::string getUserId();
    std::string getUserEmail();
    std::string getUserName();


    std::string* getToken();
private:
    bool getSessionForToken(std::string token);
    std::string sha1(std::string str);

    sqlite3 *database;
    bool db_connected;

    std::map<std::string, std::string>* cookieList;
    std::string current_token;
    bool user_loaded;

    std::string user_email;
    std::string user_name;
    int user_id;

};

#endif
