#include <string>
#include <map>
#include <iostream>

#include <boost/regex.hpp>

#include "RequestParser.h"
#include "Response.h"
#include "Model.cpp"

// #include "Controller.cpp"

#define COMPRESSION_ON true
#define CACHE_ON true

typedef std::map<std::string, std::string> AssocArray;

class Controller {
public:
    Controller();
    ~Controller();

    void run(RequestParser* req_header);
    Response* getResponse();
private:
    Response* response;
    Model* model;
    RequestParser* req_header;
    bool disable_gzip;

    std::string req_path_str;

    void ctr_assets();
    void ctr_404();
    void ctr_index();
    void ctr_login();
    void ctr_signup();
    void ctr_user();

    void ctr_api_login();
    void ctr_api_signup();
    void ctr_api_logout();

    void ctr_api_add_post();
    void ctr_api_get_posts();

    void ctr_api_get_friend_requests();
    void ctr_api_get_friend_list();
    void ctr_api_send_friend_request();
    void ctr_api_accept_friend_requests();
    void ctr_api_del_friend();

    void ctr_api_search_user();
};

Controller::Controller() {
    response = new Response();
    model = new Model();
    disable_gzip = false;

    if(CACHE_ON){
        response->setCacheOn();
    }
}

Controller::~Controller(){
    delete response;
    delete model;
}

Response* Controller::getResponse(){
    if(COMPRESSION_ON && req_header->acceptsEncoding("gzip") && !disable_gzip){
        response->setCompressionOn();
    }

    return response;
}

void Controller::run(RequestParser* req_header_p){
    req_header = req_header_p;

    char* req_type = req_header->getReqType();
    char* req_path = req_header->getReqPath();

    req_path_str = std::string(req_path);

    model->feedCookies(req_header->getCookies());

    if(boost::regex_match(req_path_str, boost::regex("/assets/([a-zA-Z0-9_\\-]+/)*(([a-zA-Z0-9_\\-]|.)+(\\.css|\\.js|\\.map|\\.ttf|\\.woff2|\\.woff|\\.svg|\\.png|\\.jpg))"))){
        this->ctr_assets();
    }
    else if(boost::regex_match(req_path_str, boost::regex("/"))){
        this->ctr_index();
    }
    else if(boost::regex_match(req_path_str, boost::regex("/login"))){
        this->ctr_login();
    }
    else if(boost::regex_match(req_path_str, boost::regex("/signup"))){
        this->ctr_signup();
    }
    else if(boost::regex_match(req_path_str, boost::regex("/user/\\d+"))){
        this->ctr_user();
    }
    else if(strcmp(req_type, "POST") == 0 && boost::regex_match(req_path_str, boost::regex("/api/login"))){
        this->ctr_api_login();
    }
    else if(strcmp(req_type, "POST") == 0 && boost::regex_match(req_path_str, boost::regex("/api/signup"))){
        this->ctr_api_signup();
    }
    else if(strcmp(req_type, "POST") == 0 && boost::regex_match(req_path_str, boost::regex("/api/logout"))){
        this->ctr_api_logout();
    }
    else if(strcmp(req_type, "POST") == 0 && boost::regex_match(req_path_str, boost::regex("/api/add_post"))){
        this->ctr_api_add_post();
    }
    else if(strcmp(req_type, "POST") == 0 && boost::regex_match(req_path_str, boost::regex("/api/get_posts"))){
        this->ctr_api_get_posts();
    }
    else if(strcmp(req_type, "POST") == 0 && boost::regex_match(req_path_str, boost::regex("/api/search_user"))){
        this->ctr_api_search_user();
    }
    else if(strcmp(req_type, "POST") == 0 && boost::regex_match(req_path_str, boost::regex("/api/get_friend_requests"))){
        this->ctr_api_get_friend_requests();
    }
    else if(strcmp(req_type, "POST") == 0 && boost::regex_match(req_path_str, boost::regex("/api/get_friend_list"))){
        this->ctr_api_get_friend_list();
    }
    else if(strcmp(req_type, "POST") == 0 && boost::regex_match(req_path_str, boost::regex("/api/send_friend_request"))){
        this->ctr_api_send_friend_request();
    }
    else if(strcmp(req_type, "POST") == 0 && boost::regex_match(req_path_str, boost::regex("/api/accept_friend_requests"))){
        this->ctr_api_accept_friend_requests();
    }
    else if(strcmp(req_type, "POST") == 0 && boost::regex_match(req_path_str, boost::regex("/api/del_friend"))){
        this->ctr_api_del_friend();
    }
    else{
        this->ctr_404();
    }
}

void Controller::ctr_index(){
    if(!model->isLoggedIn()){
        response->setRedirect("/login");
        return;
    }
    AssocArray vars;

    vars["title"] = "News Feed | VirtualSoc";
    vars["user.id"] = model->getUserId();
    vars["user.email"] = model->getUserEmail();
    vars["user.name"] = model->getUserName();
    vars["user.friend_requests"] = model->getFriendRequestsCount();


    response->loadTemplate("index.tpl", &vars);
}
void Controller::ctr_user(){
    if(!model->isLoggedIn()){
        // response->setRedirect("/login");
        // return;
        model->setGuest();
    }

    char* req_path = req_header->getReqPath();

    boost::regex expr{"(\\d+)$"};
    boost::smatch what;

    if(!boost::regex_search(std::string(req_path), what, expr)){
        this->ctr_404();
        return;
    }

    AssocArray user_info = model->getUserInfoById(atoi(what.str(1).c_str()));

    if(user_info.count("name") == 0){
        printf("User not found 2\n");
        this->ctr_404();
        return;
    }

    AssocArray vars;

    vars["title"] =  user_info.at("name") + " | VirtualSoc";
    vars["user.id"] = model->getUserId();
    vars["user.email"] = model->getUserEmail();
    vars["user.name"] = model->getUserName();
    vars["user.friend_requests"] = model->getFriendRequestsCount();

    vars["profile.name"] = user_info.at("name");
    vars["profile.post_count"] = user_info.at("post_count");
    vars["profile.id"] = user_info.at("id");


    if(user_info.at("id") == model->getUserId()){
        response->loadTemplate("profile_me.tpl", &vars);
    }
    else if(user_info.count("friend") && user_info.at("friend")[0] == '1'){
        response->loadTemplate("profile_fr.tpl", &vars);
    }
    else {
        response->loadTemplate("profile.tpl", &vars);
    }

}
void Controller::ctr_login(){
    AssocArray vars;
    vars["title"] = "Login | VirtualSoc";
    vars["user.id"] = "0";
    response->loadTemplate("login.tpl");
}
void Controller::ctr_signup(){
    AssocArray vars;
    vars["title"] = "Signup | VirtualSoc";
    vars["user.id"] = "0";
    response->loadTemplate("signup.tpl");
}

void Controller::ctr_assets(){
    std::string asset_path = "./public" + req_path_str;



    std::pair<char*, int> file_data = get_bfile_content(asset_path.c_str());
    char* file_cont = file_data.first;
    int file_size = file_data.second;

    if(!file_cont){
        this->ctr_404();
        return;
    }

    std::string data = std::string(file_cont, file_size);
    delete file_cont;

    if(strcmp(asset_path.c_str() + asset_path.size() - 3, "css") == 0){
        response->setType("css");
    }
    else if(strcmp(asset_path.c_str() + asset_path.size() - 2, "js") == 0){
        response->setType("js");
    }
    else if(strcmp(asset_path.c_str() + asset_path.size() - 3, "map") == 0){
        response->setType("map");
    }
    else if(strcmp(asset_path.c_str() + asset_path.size() - 4, "woff") == 0){
        response->setType("woff");
        disable_gzip = true;
    }
    else if(strcmp(asset_path.c_str() + asset_path.size() - 5, "woff2") == 0){
        response->setType("woff2");
        disable_gzip = true;
    }
    else if(strcmp(asset_path.c_str() + asset_path.size() - 4, "ttf") == 0){
        response->setType("ttf");
        disable_gzip = true;
    }

    response->setContent(&data);
}

void Controller::ctr_404(){
    response->setType("html");
    response->setCodeAndMsg(404, "File not found");
    response->setContent("<h2>Error 404</h2><p>File not Found :(</p>");
}



void Controller::ctr_api_login(){
    AssocArray* body_params = req_header->getBodyParams();
    AssocArray response_json;

    if(
        body_params->count("email") == 0 ||
        body_params->count("pass") == 0 ||
        body_params->at("email").size() == 0 ||
        body_params->at("pass").size() == 0
    ){
        response_json["status"] = "error";
        response_json["msg"] = "Please type an email and a password.";
    }
    else{
        if(model->loginUser(body_params->at("email"), body_params->at("pass"))){
            // SET TOKEN HERE
            std::string* the_tok = model->getToken();

            if(the_tok){
                response_json["status"] = "success";
                response_json["msg"] = "Connected successfuly.";
                response->setCookie("token", the_tok->c_str(), 60*60*24);
            }
            else {
                response_json["status"] = "error";
                response_json["msg"] = "Some weird error encountered.";
            }
        }
        else{
            response_json["status"] = "error";
            response_json["msg"] = "Wrong email or password.";
        }
    }

    response->setJsonContent(&response_json);
}


void Controller::ctr_api_signup(){
    AssocArray* body_params = req_header->getBodyParams();
    AssocArray response_json;

    if(
        body_params->count("name") == 0 ||
        body_params->count("email") == 0 ||
        body_params->count("pass") == 0 ||
        body_params->count("passc") == 0 ||
        body_params->at("email").size() == 0 ||
        body_params->at("name").size() == 0 ||
        body_params->at("pass").size() == 0 ||
        body_params->at("passc").size() == 0
    ){
        response_json["status"] = "error";
        response_json["msg"] = "Please complete all the fields.";
    }
    else if(body_params->at("pass") != body_params->at("passc")){
        response_json["status"] = "error";
        response_json["msg"] = "Passwords don't match.";
    }
    else if(body_params->at("pass").size() < 6){
        response_json["status"] = "error";
        response_json["msg"] = "Password needs to have at least 6 characters.";
    }
    else if(model->emailExists(body_params->at("email"))){
        response_json["status"] = "error";
        response_json["msg"] = "Email already exists.";
    }
    else if(!model->signupUser(body_params->at("email"), body_params->at("name"), body_params->at("pass"))) {
        response_json["status"] = "error";
        response_json["msg"] = "Error signing up. Please try again later.";
    }
    else {
        if(model->loginUser(body_params->at("email"), body_params->at("pass"))){
            std::string* the_tok = model->getToken();
            response->setCookie("token", the_tok->c_str(), 60*60*24);
            response_json["msg"] = "Successfuly signed up.";
        }
        else{
            response_json["msg"] = "Successfuly signed up. Now please login.";
        }
        response_json["status"] = "success";
    }

    response->setJsonContent(&response_json);
}
void Controller::ctr_api_logout(){
    model->logoutUser();
    response->setCookie("token", "0", 0);
}
void Controller::ctr_api_add_post(){
    AssocArray* body_params = req_header->getBodyParams();
    AssocArray response_json;

    response_json["status"] = "error";

    if(!model->isLoggedIn()){
        response_json["msg"] = "You need to be logged in to add a post. Please refresh your page.";
    }
    else if(
        body_params->count("content") == 0 ||
        body_params->count("visibility") == 0 ||
        body_params->at("content").size() == 0
    ){
        response_json["msg"] = "Please complete all the fields.";
    }
    else if(
        body_params->at("visibility").c_str()[0] != '1' &&
        body_params->at("visibility").c_str()[0] != '2' &&
        body_params->at("visibility").c_str()[0] != '3'
    ){
        response_json["msg"] = "Invalid post visibility.";
    }
    else if(body_params->at("content").size() < 3){
        response_json["msg"] = "Please type at least 3 characters.";
    }
    else {
        if(model->addPost(body_params->at("content"), body_params->at("visibility").c_str()[0] - '0')){
            response_json["msg"] = "Post added successfuly.";
            response_json["status"] = "success";
        }
        else {
            response_json["msg"] = "Error adding post. Please try again later.";
        }
    }

    response->setJsonContent(&response_json);
}

void Controller::ctr_api_get_posts(){
    AssocArray* body_params = req_header->getBodyParams();

    if(!model->isLoggedIn()){
        AssocArray response_json;
        response_json["msg"] = "You need to be logged in to do this. Please refresh your page.";
        response_json["status"] = "error";
        response->setJsonContent(&response_json);
        return;
    }

    if(body_params->count("user_id")){
        int uid = atoi(body_params->at("user_id").c_str());
        response->setJsonContent(model->getPosts(uid));
        return;
    }

    response->setJsonContent(model->getPosts());
}
void Controller::ctr_api_search_user(){
    AssocArray* body_params = req_header->getBodyParams();
    AssocArray response_json;

    if(!model->isLoggedIn()){
        response_json["msg"] = "You need to be logged in to do this. Please refresh your page.";
        response_json["status"] = "error";
        response->setJsonContent(&response_json);
        return;
    }
    else if(
        body_params->count("text") == 0 ||
        body_params->at("text").size() == 0
    ){
        response_json["msg"] = "Please type something to search for.";
        response_json["status"] = "error";
        response->setJsonContent(&response_json);
        return;
    }

    response->setJsonContent(model->searchUser(body_params->at("text")));
}
void Controller::ctr_api_del_friend(){
    AssocArray* body_params = req_header->getBodyParams();
    AssocArray response_json;

    response_json["status"] = "error";
    if(!model->isLoggedIn()){
        response_json["msg"] = "You need to be logged in do this. Please refresh your page.";
    }
    else if(
        body_params->count("pid") == 0 ||
        body_params->at("pid").size() == 0
    ){
        response_json["msg"] = "Please provide a valid user id.";
    }
    else if(!model->deleteFriend(atoi(body_params->at("pid").c_str()))){
        response_json["msg"] = "Error deleting friend, please try again later.";
    }
    else {
        response_json["msg"] = "Friend deleted successfuly.";
        response_json["status"] = "success";
    }

    response->setJsonContent(&response_json);
}
void Controller::ctr_api_get_friend_requests(){
    AssocArray* body_params = req_header->getBodyParams();
    AssocArray response_json;

    if(!model->isLoggedIn()){
        response_json["msg"] = "You need to be logged in to do this. Please refresh your page.";
        response_json["status"] = "error";
        response->setJsonContent(&response_json);
        return;
    }
    response->setJsonContent(model->getFriendRequests());
}
void Controller::ctr_api_send_friend_request(){
    AssocArray* body_params = req_header->getBodyParams();
    AssocArray response_json;

    response_json["status"] = "error";

    if(!model->isLoggedIn()){
        response_json["msg"] = "You need to be logged in to do this. Please refresh your page.";
        response->setJsonContent(&response_json);
        return;
    }
    else if(!model->sendFriendRequest(atoi(body_params->at("pid").c_str()))){
        response_json["msg"] = "Error sending friend request, please try again later.";
    }
    else{
        response_json["msg"] = "Friend request sent successfuly.";
        response_json["status"] = "success";
    }

    response->setJsonContent(&response_json);
}
void Controller::ctr_api_accept_friend_requests(){
    AssocArray* body_params = req_header->getBodyParams();
    AssocArray response_json;

    response_json["status"] = "error";

    if(!model->isLoggedIn()){
        response_json["msg"] = "You need to be logged in to add a post. Please refresh your page.";
        response->setJsonContent(&response_json);
        return;
    }
    else if(!model->acceptFriendRequest(atoi(body_params->at("pid").c_str()))){
        response_json["msg"] = "Error accepting friend request, please try again later.";
    }
    else{
        response_json["msg"] = "Friend added successfuly.";
        response_json["status"] = "success";
    }

    response->setJsonContent(&response_json);
}


void Controller::ctr_api_get_friend_list(){
    AssocArray* body_params = req_header->getBodyParams();
    AssocArray response_json;

    if(!model->isLoggedIn()){
        response_json["msg"] = "You need to be logged in to do this. Please refresh your page.";
        response_json["status"] = "error";
        response->setJsonContent(&response_json);
        return;
    }
    response->setJsonContent(model->getFriendList());
}
