#include <string>

#include "Response.cpp"

#include "MainHandler.h"


MainHandler::MainHandler(){
    request_header = new RequestParser();
    app_controller = new Controller();
}

MainHandler::~MainHandler(){
    delete request_header;
    delete app_controller;
}

const char* MainHandler::getResponseStr(){
    return res_str;
}
int MainHandler::getResponseLen(){
    return res_len;
}

void MainHandler::handleRequest(char* req_msg){
    char* req_cursor = req_msg;

   request_header->beginParsing(req_cursor);

   app_controller->run(request_header);

   std::string* res_msg = app_controller->getResponse()->getResponseStr();

    res_str = res_msg->c_str();
    res_len = res_msg->length();

    printf("[%d] %s %s\n", app_controller->getResponse()->getCode(), request_header->getReqType(), request_header->getReqPath());
}
