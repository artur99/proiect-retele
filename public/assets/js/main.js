$(document)
.on('submit', '#form_login', function(e){
    var email = $(this).find("#login_email").val().trim();
    var pass = $(this).find("#login_pass").val().trim();

    Swal({title: 'Loading...', onBeforeOpen: () => Swal.showLoading()});
    $.post("/api/login", {email: email, pass: pass})
    .done(function(body){
        // console.log(body)
        if(typeof body != 'object' || typeof body.status == 'undefined'){
            swal("Error", "Failed contacting server.", "error");
        }
        else{
            if(body.status == 'success'){
                swal("Success", body.msg ? body.msg : "All done.", "success")
                .then(result => {
                    window.location.href = '/';
                })
            }
            else {
                swal("Error", body.msg ? body.msg : "Unknwon error.", "error");
            }
        }
    })
    .fail(function(err){
        swal("Error", "Failed contacting server.", "error");
    })

    return false;
})
.on('submit', '#form_signup', function(e){
    var name = $(this).find("#signup_name").val().trim();
    var email = $(this).find("#signup_email").val().trim();
    var pass = $(this).find("#signup_pass").val().trim();
    var passc = $(this).find("#signup_passc").val().trim();

    Swal({title: 'Loading...', onBeforeOpen: () => Swal.showLoading()});
    $.post("/api/signup", {name: name, email: email, pass: pass, passc: passc})
    .done(function(body){
        // console.log(body)
        if(typeof body != 'object' || typeof body.status == 'undefined'){
            swal("Error", "Failed contacting server.", "error");
        }
        else{
            if(body.status == 'success'){
                swal("Success", body.msg ? body.msg : "All done.", "success")
                .then(result => {
                    window.location.href = '/';
                })
            }
            else {
                swal("Error", body.msg ? body.msg : "Unknwon error.", "error");
            }
        }
    })
    .fail(function(err){
        swal("Error", "Failed contacting server.", "error");
    })

    return false;
})
.on('submit', '#form_add_post', function(e){
    var post_content = $(this).find(".input_content").val().trim();
    var post_visibility = $(".input_visibility option:selected").val();

    Swal({title: 'Loading...', onBeforeOpen: () => Swal.showLoading()});
    $.post("/api/add_post", {content: post_content, visibility: post_visibility})
    .done(function(body){
        if(typeof body != 'object' || typeof body.status == 'undefined'){
            swal("Error", "Failed contacting server.", "error");
        }
        else{
            if(body.status == 'success'){
                swal("Success", body.msg ? body.msg : "All done.", "success")
                .then(result => {
                    window.location.reload();
                })
            }
            else {
                swal("Error", body.msg ? body.msg : "Unknwon error.", "error");
            }
        }
    })
    .fail(function(err){
        swal("Error", "Failed contacting server.", "error");
    })

    return false;
})
.on('click', '.logout_btn', function(e){
    e.preventDefault();

    Swal({title: 'Loading...', onBeforeOpen: () => Swal.showLoading()});
    $.post("/api/logout")
    .done(function(){
        window.location.href = '/login';
    })
    .fail(function(){
        window.location.href = '/login';
    })
})
.ready(function(){
    if(user_id == 0){
        $(".logout_btn").html("Login");
        $("a[href='/user/0']").attr("href", "/login");
    }

    if($(".posts-container").length && !$(".posts-container").data('loaded')){
        let data = {};
        let user_id;
        if($(".posts-container").data("userid")){
            user_id = $(".posts-container").data("userid");
            data['user_id'] = user_id;
        }

        $.post("/api/get_posts", data)
        .done(function(body){
            var thtml = "";

            if(!body || typeof body['posts'] == "undefined"){
                $(".posts-container").html("Failed contacting server for news feed...");
            }
            else {
                var posts = body['posts']
                if(posts.length == 0){
                    thtml += "No recent posts...";
                }
                else{
                    for(var post of posts){
                        thtml += '<div class="post">';
                        thtml += '<h6>';
                        if(post['user_id'] == glob_user_id){
                            thtml += '<a href="/user/' + parseInt(post['user_id']) + '">' + html_escape(post['user_name']) + ' (You)</a> ';
                        }
                        else{
                            thtml += '<a href="/user/' + parseInt(post['user_id']) + '">' + html_escape(post['user_name']) + '</a> ';
                        }
                        thtml += '<span class="visibility"><i class="material-icons">';
                        if(post['visibility'] == '1') thtml += 'public';
                        else if(post['visibility'] == '2') thtml += 'people';
                        else if(post['visibility'] == '3') thtml += 'lock';
                        thtml += '</i></span> ';
                        thtml += '<small>(' + unix_to_time_ago(post['post_time']) + ' ago)</small>';
                        thtml += '</h6>';
                        thtml += '<div class="post-content">' + html_escape(post['post']) + '</div>';
                        thtml += '</div>';
                    }
                }
                $(".posts-container").html(thtml);
            }

            $(".posts-container").data('loaded', 'true');
        })
        .fail(function(err){
            $(".posts-container").html("Failed contacting server for news feed...");
            $(".posts-container").data('loaded', 'true');
        })
    }

})
.on('submit', '#form_search', function(e){
    var search_str = $(this).find(".search_input").val().trim();
    var post_visibility = $(".input_visibility option:selected").val();

    Swal({title: 'Loading...', onBeforeOpen: () => Swal.showLoading()});
    $.post("/api/search_user", {text: search_str})
    .done(function(body){
        if(typeof body != 'object'){
            swal("Error", "Failed contacting server.", "error");
        }
        else{
            if(typeof body.results != 'undefined'){
                var thtml = '';
                if(body.results.length == 0){
                    thtml = '<em>No users found...</em>';
                }
                else{
                    thtml += 'Found following users:<br>';
                    thtml += '<div class="list-group inside_swal">';
                    for(var user of body.results){
                        thtml += '<a class="list-group-item list-group-item-action" href="/user/' + parseInt(html_escape(user.user_id)) + '">' + html_escape(user.user_name);
                        if(user.friends === '1'){
                            thtml += ' <i class="material-icons">person</i>';
                        }
                        else if(user.user_id == user_id){
                            thtml += ' <i class="material-icons">home</i>';
                        }
                        thtml += '</a>';
                    }
                    thtml += '</div>';
                }
                swal({
                    title: "Results",
                    html: thtml
                });
            }
            else {
                swal("Error", body.msg ? body.msg : "Unknwon error.", "error");
            }
        }
    })
    .fail(function(err){
        swal("Error", "Failed contacting server.", "error");
    })

    return false;
})
.on('click', '.add_friend', function(e){
    e.preventDefault();
    var pid = $(this).data('id');

    Swal({title: 'Loading...', onBeforeOpen: () => Swal.showLoading()});
    $.post("/api/send_friend_request", {pid: pid})
    .done(function(body){
        if(typeof body != 'object' || typeof body.status == 'undefined'){
            swal("Error", "Failed contacting server.", "error");
        }
        else{
            if(body.status == 'success'){
                swal("Success", body.msg ? body.msg : "All done.", "success")
                .then(result => {
                    window.location.reload();
                })
            }
            else {
                swal("Error", body.msg ? body.msg : "Unknwon error.", "error");
            }
        }
    })
    .fail(function(err){
        swal("Error", "Failed contacting server.", "error");
    })
})
.on('click', '.delete_friend', function(e){
    e.preventDefault();
    var pid = $(this).data('id');

    Swal({title: 'Loading...', onBeforeOpen: () => Swal.showLoading()});
    $.post("/api/del_friend", {pid: pid})
    .done(function(body){
        if(typeof body != 'object' || typeof body.status == 'undefined'){
            swal("Error", "Failed contacting server.", "error");
        }
        else{
            if(body.status == 'success'){
                swal("Success", body.msg ? body.msg : "All done.", "success")
                .then(result => {
                    window.location.reload();
                })
            }
            else {
                swal("Error", body.msg ? body.msg : "Unknwon error.", "error");
            }
        }
    })
    .fail(function(err){
        swal("Error", "Failed contacting server.", "error");
    })
})
.on('click', '.view_friend_requests', function(e){
    e.preventDefault();

    Swal({title: 'Loading...', onBeforeOpen: () => Swal.showLoading()});
    $.post("/api/get_friend_requests")
    .done(function(body){
        if(typeof body.requests != 'undefined'){
            var thtml = '';
            if(body.requests.length == 0){
                thtml = '<em>No friend requests...</em>';
            }
            else{
                thtml += 'You have these friend requests:<br>';
                thtml += '<div class="list-group inside_swal">';
                for(var user of body.requests){
                    thtml += '<a class="list-group-item list-group-item-action accept_request" href="#" data-id="' + user.user_id + '" data-name="' + html_escape(user.name) + '">' + html_escape(user.name);
                    thtml += '</a>';
                }
                thtml += '</div>';
            }
            swal({
                title: "Friend requests",
                html: thtml
            });
        }
        else {
            if(body.status == 'success'){
                swal("Success", body.msg ? body.msg : "All done.", "success");
            }
            else {
                swal("Error", body.msg ? body.msg : "Unknwon error.", "error");
            }
        }
    })
    .fail(function(err){
        swal("Error", "Failed contacting server.", "error");
    })
})
.on('click', '.accept_request', function(e){
    e.preventDefault();
    var uid = $(this).data('id');
    var name = $(this).data('name');
    Swal({
        title: 'Are you sure?',
        text: "Are you sure you want to accet the friend request from " + name + "?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, accept it!'
    }).then((result) => {
      if (result.value) {
          Swal({title: 'Loading...', onBeforeOpen: () => Swal.showLoading()});
          $.post("/api/accept_friend_requests", {pid: uid})
          .done(function(body){
              if(typeof body != 'object' || typeof body.status == 'undefined'){
                  swal("Error", "Failed contacting server.", "error");
              }
              else{
                  if(body.status == 'success'){
                      swal("Success", body.msg ? body.msg : "All done.", "success")
                      .then(result => {
                          window.location.href = '/';
                      })
                  }
                  else {
                      swal("Error", body.msg ? body.msg : "Unknwon error.", "error");
                  }
              }
          })
          .fail(function(err){
              swal("Error", "Failed contacting server.", "error");
          })
      }
    })
})
.on('click', '.view_friends', function(e){
    e.preventDefault();

    Swal({title: 'Loading...', onBeforeOpen: () => Swal.showLoading()});
    $.post("/api/get_friend_list")
    .done(function(body){
        if(typeof body != 'object'){
            swal("Error", "Failed contacting server.", "error");
        }
        else{
            if(typeof body.friend_list != 'undefined'){
                var thtml = '';
                if(body.friend_list.length == 0){
                    thtml = '<em>You have no friends yet. :( Try and search for a friend using the top-bar search form...</em>';
                }
                else{
                    thtml += 'These are your friends:<br>';
                    thtml += '<div class="list-group inside_swal">';
                    for(var user of body.friend_list){
                        thtml += '<a class="list-group-item list-group-item-action" href="/user/' + parseInt(html_escape(user.user_id)) + '">' + html_escape(user.name);
                        thtml += '</a>';
                    }
                    thtml += '</div>';
                }
                swal({
                    title: "Friends",
                    html: thtml
                });
            }
            else {
                swal("Error", body.msg ? body.msg : "Unknwon error.", "error");
            }
        }
    })
    .fail(function(err){
        swal("Error", "Failed contacting server.", "error");
    })
})


var html_escape = function(html){
    return html
        .replace(/&/g, '&amp;')
        .replace(/>/g, '&gt;')
        .replace(/</g, '&lt;')
        .replace(/'/g, '&#39;')
        .replace(/\n/g, '<br>\n')
        .replace(/"/g, '&quot;');
}

var unix_to_time_ago = function(unix_time){
    let diff = Math.abs((Date.now() / 1000 | 0) - unix_time);

    if(diff < 60){
        return diff + ' sec';
    }
    diff = Math.floor(diff / 60);
    if(diff < 60){
        return diff + ' min' + (diff > 1 ? 's' : '');
    }
    diff = Math.floor(diff / 60);
    if(diff < 24){
        return diff + ' hr' + (diff > 1 ? 's' : '');
    }
    diff = Math.floor(diff / 24);
    if(diff < 30){
        return diff + ' day' + (diff > 1 ? 's' : '');
    }
    diff = Math.floor(diff / 30);
    return diff + ' month' + (diff > 1 ? 's' : '');
}
