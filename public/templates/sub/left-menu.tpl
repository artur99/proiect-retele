<div class="card">
    <div class="card-body">
        <nav class="nav left-menu">
            <a class="nav-link active" href="/"><i class="material-icons">dashboard</i> News Feed</a>
            <a class="nav-link" href="/user/{{user.id}}"><i class="material-icons">perm_identity</i> My Profile</a>
            <a class="nav-link view_friends" href="#"><i class="material-icons">people</i> My friends</a>
        </nav>
    </div>
</div>
