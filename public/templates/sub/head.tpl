<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<link rel="stylesheet" href="/assets/vendor/css/bootstrap.min.css">
<link rel="stylesheet" href="/assets/vendor/css/sweetalert2.min.css">
<link rel="stylesheet" href="/assets/vendor/materialicons/material-icons.css">
<link rel="stylesheet" href="/assets/css/main.css">
<title>{{ title }}</title>
