<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container">
        <a class="navbar-brand mm-logo" href="/">Virtual<span>Soc</span></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-expanded="true">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="main-menu">
            <form class="form-inline mr-auto" id="form_search">
                <input class="form-control mr-sm-2 search_input" type="search" placeholder="Search..." aria-label="Search" />
            </form>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/user/{{user.id}}">{{user.name}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link view_friend_requests" href="#"><i class="material-icons">group</i> <span class="badge badge-light">{{user.friend_requests}}</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link logout_btn" href="#">Logout</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
