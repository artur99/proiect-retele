<script src="/assets/vendor/js/jquery.min.js" charset="utf-8"></script>
<script src="/assets/vendor/js/popper.min.js" charset="utf-8"></script>
<script src="/assets/vendor/js/bootstrap.min.js" charset="utf-8"></script>
<script src="/assets/vendor/js/sweetalert2.min.js" charset="utf-8"></script>
<script src="/assets/js/main.js" charset="utf-8"></script>
<script>
    var user_id = 0{{user.id}};
    var glob_user_id = user_id;
</script>
