<!DOCTYPE html>
<html lang="en">
<head>
    <title>Signup | VirtualSoc</title>
    {% include 'sub/head.tpl' %}
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 offset-md-4">
                <div class="centered-logo">
                    Virtual<span>Soc</span>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title text-center">Signup</h3>
                            <form id="form_signup">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="signup_name" placeholder="Your full name...">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" id="signup_email" placeholder="Email address...">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="signup_pass" placeholder="Password...">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="signup_passc" placeholder="Confirm password...">
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">Signup</button>
                            </form>
                            <div class="text-center">or</div>
                            <a href="/login" class="btn btn-secondary btn-block">Login</a>
                    </div>
                </div>
            </div>
        </div>
        {% include 'sub/copy1.tpl' %}
    </div>



    {% include 'sub/scripts.tpl' %}
</body>
</html>
