<!DOCTYPE html>
<html lang="en">
<head>
    {% include 'sub/head.tpl' %}
</head>
<body class="userarea">
    {% include 'sub/main-menu.tpl' %}
    <div class="container page-container">
        <div class="row">
            <div class="col-md-3">
                {% include 'sub/left-menu.tpl' %}
            </div>
            <div class="col-md-9 right-main-container">
                <div class="card">
                    <div class="card-body">
                        <form action="" id="form_add_post">
                            <div class="form-group">
                                <!-- <h5><label for="input_add_post">Type a new post...</label></h5> -->
                                <textarea class="form-control input_content" rows="3" required placeholder="Hello, {{user.name}}! Write a new post..."></textarea>
                            </div>
                            <div class="form-inline float-left">
                                <select class="custom-select float-left input_visibility" class="input_visibility">
                                    <option value="1">Public</option>
                                    <option value="2">Friends</option>
                                    <option value="3">Only Me</option>
                                </select>
                            </div>
                            <button class="btn btn-primary float-right" type="submit">Add Post</button>
                        </form>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h5>Recent posts</h5>
                        <br>
                        <div class="posts-container" data-loaded="false">Loading...</div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    {% include 'sub/scripts.tpl' %}
</body>
</html>
