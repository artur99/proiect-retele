<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login | VirtualSoc</title>
    {% include 'sub/head.tpl' %}
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 offset-md-4">
                <div class="centered-logo">
                    Virtual<span>Soc</span>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title text-center">Login</h3>
                            <form id="form_login">
                                <div class="form-group">
                                    <input type="email" class="form-control" id="login_email" required placeholder="Email address...">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="login_pass" required placeholder="Password...">
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">Login</button>
                            </form>
                            <div class="text-center">or</div>
                            <a href="/signup" class="btn btn-secondary btn-block">Signup</a>
                    </div>
                </div>
            </div>
        </div>
        {% include 'sub/copy1.tpl' %}
    </div>



    {% include 'sub/scripts.tpl' %}
</body>
</html>
