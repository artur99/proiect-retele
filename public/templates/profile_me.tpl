<!DOCTYPE html>
<html lang="en">
<head>
    {% include 'sub/head.tpl' %}
</head>
<body class="userarea">
    {% include 'sub/main-menu.tpl' %}
    <div class="container page-container">
        <div class="row">
            <div class="col-md-3">
                {% include 'sub/left-menu.tpl' %}
            </div>
            <div class="col-md-9 right-main-container">
                <div class="card">
                    <div class="card-body profile-info">
                        <h5>{{profile.name}} <small>(<em>User has {{profile.post_count}} posts</em>)</small></h5>
                        <p><strong>This is your profile.</strong></p>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h5>User posts</h5>
                        <br>
                        <div class="posts-container" data-userid="{{profile.id}}" data-loaded="false">Loading...</div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    {% include 'sub/scripts.tpl' %}
</body>
</html>
