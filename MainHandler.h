#ifndef MAIN_HANDLER_H
#define MAIN_HANDLER_H

#include "RequestParser.cpp"
#include "Controller.cpp"

class MainHandler {
private:
    RequestParser* request_header;
    Controller* app_controller;
public:
    MainHandler();
    ~MainHandler();
    void handleRequest(char* req_msg);

    const char* getResponseStr();
    int getResponseLen();

    int res_len = 0;
    const char* res_str;
};

#endif
